/**
 * Created by Leonard Cheong on 28/3/2016.
 */

// test //

var express = require("express");
var app = express();

// [GET (http method) / (resource)
app.get("/", function(req, res) {

        //Status
        res.status(202);
        // MIME type
        res.type("text/plain");
        res.send("The current time is " + (new Date()));
    }
);


app.get("/hello", function(req, res){
    res.status(202);
    res.type("text/plain");
    res.send("HELLO");
}
);

//3000: Port Number
app.listen(3000, function() {
    console.info("Application started on port 3000");
});
